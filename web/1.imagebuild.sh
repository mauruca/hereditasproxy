#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]
  then
    echo "./imagebuild.sh <type: base,io,web>"
    exit 1
fi

type=$1

# base conf
version="$(cat version)"

cp _base.nginx.conf nginx.conf

if [ "$1" == "io" ]
then
    cp _hereditasio.nginx.conf nginx.conf
elif [ "$1" == "web" ]
then
    cp _hereditasweb.nginx.conf nginx.conf
fi

docker build --force-rm -t hereditas/proxy:$type.$version .
docker build --force-rm -t hereditas/proxy:$type .    

docker images hereditas/proxy

rm nginx.conf