#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

version="$(cat version)"

if [ "$1" == "io" ]
then
    docker push hereditas/proxy:$1.$version
    docker push hereditas/proxy:$1
elif [ "$1" == "web" ]
then
    docker push hereditas/proxy:$1.$version
    docker push hereditas/proxy:$1    
else
    docker push hereditas/proxy:latest
fi