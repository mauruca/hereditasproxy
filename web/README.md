# Running the container

Must define some environment variables for base server:

${LISTEN_PORT} = just the port or port + deferred; Ex: LISTEN_PORT=80 or LISTEN_PORT=80 deferred
${DOMAIN_FILTER} = domain name; Ex: DOMAIN_FILTER=hereditas.in
${BACKEND_URI_SERVICE} = https://servicename:port; Ex: BACKEND_URI_SERVICE=https://hereditas:8080
