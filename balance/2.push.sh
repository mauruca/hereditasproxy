#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

tagversion="$(awk '{print $1}' version)"

docker push hereditas/proxy:balance-$tagversion
docker push hereditas/proxy:latest