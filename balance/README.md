# Heretidas Proxy

HAProxy configuration to work as a load balance and routing name.

## Key Features
* Load balance as a service in your cluster.
* Routing name to specific service cluster port.
* SSL for all services.

## How to build and push the proxy
```
Copy the certificates path to proxy build path and run the build script as follows:
```bash
$ build.sh <reponame>
```
The build will create an alpine updated version and a tagged build.

If you need to push the files to docker hub run the command:
```bash
$ push.sh <reponame>
```

### Run
```bash
$ docker service create --name haprxy --mount type=bind,source=/etc/crts,destination=/etc/crts -p 80:80 -p 443:443 --detach=true --sysctl net.ipv4.ip_unprivileged_port_start=0 --replicas 2 <reponame>/proxy:$version
```